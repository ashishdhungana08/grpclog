const PROTO_PATH = "./customers.proto";   //proto path
const grpc = require('grpc');             //grpc
const protoloader = require('@grpc/proto-loader');  //grpc loader   
const { groupCollapsed } = require('console');
const uuidv4 = require('uuid');         //id


const packageDefination = protoloader.loadSync(PROTO_PATH,{    // load from proto path
    keepCase:true,
    longs:String,
    enums:String,
    arrays:true
}) ; 

const customersProto = grpc.loadPackageDefinition(packageDefination);  // make use of packagedefination

const server = new server();      //creae a server
const customers = [                //add customer with id name age and address as defined at porto
    {
        id: "a68b823c-7ca6-44bc-b721-fb4d5312cafc",
        name: "John Bolton",
        age: 23,
        address: "Address 1"
    },
    {
        id: "34415c7c-f82d-4e44-88ca-ae2a1aaa92b7",
        name: "Mary Anne",
        age: 45,
        address: "Address 2"
    } ];   


    server.addService(customersProto.CustomerService.service,{    // addservices to customer  
            GetAll:(_, result)=>{
                result(null,customers);
            },
            Get: (req,result) =>{
                let customer = customers.find(req.params.id);
                if (customer) {
                        res.send(result); 
                        console.log(result);
                } else{ 
                    console.log("customers not found");

                }
            }, 
            Insert: (req, result) => {
                let customer = req.request;
                customer.id = uuidv4();
                customers.push(customer);
                result(null, customer);
            }, 


    }); 

    server.bind("127.0.0.1:30043",grpc.ServerCredentials.createInsecure()); 
    console.log("server running on 127.0.0.1:30043 "); 
    server.start()  