const client = require('./client');
const path =require('path');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();   


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); 


app.get("/get", (req, res) => {
    client.getAll(null, (err, data) => {
       if (err) {
           console.log(err);
       } else {
            res.send(data);           
       }
    });
});  


app.post("/save", (req, res) => {
    let newCustomer = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address
    };

    client.insert(newCustomer, (err, data) => {
        if (err) 
        console.log("error",+err);;

        console.log("Customer created successfully", data);

    });
});
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log("Server running at port %d", PORT);
})