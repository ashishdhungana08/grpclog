const PROTO_PATH = "./customers.proto"; 
const grpc = require('grpc');
const protoloader = require('@grpc/proto-loader');  

const packageDefinition = protoloader.loadSync(PROTO_PATH,{  // sync protos
    keepCase:true,
    longs:String,
    enums:String,
    arrays:true
});

const CustomerService = grpc.loadPackageDefinition(packageDefinition).CustomerService;
const client = new CustomerService(
    "localhost:30043",
    grpc.credentials.createInsecure()
);

module.exports = client;